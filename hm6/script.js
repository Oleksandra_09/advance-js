

const button = document.querySelector(".btn");
button.addEventListener("click", myLocation);


async function myLocation() {
  try {
    const { ip } = await (await fetch("http://api.ipify.org/?format=json")).json();
    const locationData = await (await fetch(`http://ip-api.com/json/${ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`)).json();
    displayLocationData(locationData);
  } catch (error) {
    console.log("Помилка:", error);
  }
}

function displayLocationData(data) {
  const locationContainer = document.querySelector(".container");
  locationContainer.innerHTML = `
    <p>Континент: ${data.continent}</p>
    <p>Країна: ${data.country}</p>
    <p>Регіон: ${data.region}</p>
    <p>Місто: ${data.city}</p>
    <p>Район: ${data.district}</p>
  `;
}
